% Table relative permeability model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DARSim 2 Reservoir Simulator
%Author: Yuhang Wang
%TU Delft
%Created: 6 Jan 2021
%Last modified: 14 July 2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef relperm_model_table < relperm_model
    properties
        krg_Sg_table
        nonwetting_imbibation_table
        kro_Sg_table
        dkrg_Sg_table
        dkro_Sg_table
        
        Pc_drainage_Sg_table
        dPc_drainage_Sg_table
        Pc_imbibation_Sg_table
        dPc_imbibation_Sg_table
        
        % hysteresis parameters
        % safeguard_S_label
        % transport_label
        kr
        dkr
        % Pc
        % dPc
        scanning_curve_Sg
        scanning_curve_kr
        scanning_curve_dkr
        scanning_curve_Pc
        scanning_curve_dPc
        reverse_point
        process_label % 0: primary drainage; 1: scanning curve
        % immobile_label; % 0; mobile; 1: immobile
        Sg_r % residual saturation of gas phase
        
        C % Land trapping coefficient
        S_gcr % critical gas phase saturation
        S_gimax % maximum gas phase saturation
        S_gtmax % maximum residual saturation of gas phase
        
        % parameters used to update Z
        % rho_l
        % rho_v
        % x_g_l
        % x_g_v
        
        immobile_label
          
    end
    methods
        function obj = relperm_model_table(SimulationInput, Sr)
            % allocate memory
            N = SimulationInput.ReservoirProperties.Grid.N_ActiveCells;
            
            % obj.safeguard_S_label = zeros(N,1);
            % obj.transport_label = zeros(N,1);
            
            obj.kr = zeros(N,2);
            obj.dkr = zeros(N,2);
            % obj.Pc = zeros(N,1);
            % obj.dPc = zeros(N,1);
            
            obj.scanning_curve_Sg = zeros(N,20);
            obj.scanning_curve_kr = zeros(N,20);
            obj.scanning_curve_dkr = zeros(N,20);
            obj.scanning_curve_Pc = zeros(N,20);
            obj.scanning_curve_dPc = zeros(N,20);

            obj.reverse_point = zeros(N,1) + (1-Sr(2));
            obj.process_label = zeros(N,1);
            obj.immobile_label = zeros(N,1);
            obj.Sg_r = zeros(N,1) + Sr(1);
            
            % obj.rho_l = zeros(N,1);
            % obj.rho_v = zeros(N,1);
            % obj.x_g_l = zeros(N,1);
            % obj.x_g_v = zeros(N,1);
            
            % assign values to constant parameters
            % obj.C = 45/28;
            % obj.S_gcr = 0.0;
            obj.S_gimax = 1-Sr(2);
            % obj.S_gtmax = obj.S_gcr + (obj.S_gimax-obj.S_gcr)/(1+obj.C*(obj.S_gimax-obj.S_gcr)); % Land's model
            obj.S_gtmax = obj.S_gimax/2; % linear model
            
            % relations are functions of resident wetting phase;
            % read from LookupTable
            wetting_kr=[];
            nonwetting_drainage_kr=[];
            nonwetting_imbibation_kr=[];
            Pc_drainage=[];
            Pc_imbibation=[];
            
%             currentfolder=pwd;
%             cd(SimulationInput.Directory);
%             fid = fopen('LookupTable.txt','r');
%             
%             while true
%                 thisline = fgetl(fid);
%                 if (strcmp(thisline, 'end')) break; end
%                 
%                 if thisline(1) == '-'
%                     keyword = thisline(3:end);
%                     if (strcmp(keyword, 'wetting_kr'))
%                         thisline = fgetl(fid);
%                         while ~isempty(thisline)
%                             wetting_kr =[wetting_kr; sscanf(thisline,'%f',[1 2])];
%                             thisline = fgetl(fid);
%                         end
%                     end
%                     if (strcmp(keyword, 'nonwetting_drainage_kr'))
%                         thisline = fgetl(fid);
%                         while ~isempty(thisline)
%                             nonwetting_drainage_kr =[nonwetting_drainage_kr; sscanf(thisline,'%f',[1 2])];
%                             thisline = fgetl(fid);
%                         end
%                     end
%                     if (strcmp(keyword, 'nonwetting_imbibation_kr'))
%                         thisline = fgetl(fid);
%                         while ~isempty(thisline)
%                             nonwetting_imbibation_kr =[nonwetting_imbibation_kr; sscanf(thisline,'%f',[1 2])];
%                             thisline = fgetl(fid);
%                         end
%                     end
%                     if (strcmp(keyword, 'Pc_drainage'))
%                         thisline = fgetl(fid);
%                         while ~isempty(thisline)
%                             Pc_drainage =[Pc_drainage; sscanf(thisline,'%f',[1 2])];
%                             thisline = fgetl(fid);
%                         end
%                     end
%                     if (strcmp(keyword, 'Pc_imbibation'))
%                         thisline = fgetl(fid);
%                         while ~isempty(thisline)
%                             Pc_imbibation =[Pc_imbibation; sscanf(thisline,'%f',[1 2])];
%                             thisline = fgetl(fid);
%                         end
%                     end
%                 end
%                 
%             end
%             fclose(fid);
%             cd(currentfolder);
        
            %% relative permeability
            % convert relations to functions of S_g;
            wetting_kr(:,1) = 1.0 - wetting_kr(:,1);
            nonwetting_drainage_kr(:,1) = 1.0 - nonwetting_drainage_kr(:,1);
            nonwetting_imbibation_kr(:,1) = 1.0 - nonwetting_imbibation_kr(:,1);
            
            wetting_kr = flip(wetting_kr); % stored in ascending order
            nonwetting_imbibation_kr = flip(nonwetting_imbibation_kr); % stored in ascending order
            nonwetting_drainage_kr = flip(nonwetting_drainage_kr); % stored in ascending order
            
            % nonwetting_drainage_table = griddedInterpolant(nonwetting_drainage(:,1),nonwetting_drainage(:,2));
            obj.nonwetting_imbibation_table = griddedInterpolant(nonwetting_imbibation_kr(:,1),nonwetting_imbibation_kr(:,2));
                          
            obj.krg_Sg_table = griddedInterpolant(nonwetting_drainage_kr(:,1),nonwetting_drainage_kr(:,2));
            obj.kro_Sg_table = griddedInterpolant(wetting_kr(:,1),wetting_kr(:,2));
            
            dkrg_Sg_value = (nonwetting_drainage_kr(2:end,2)-nonwetting_drainage_kr(1:end-1,2))./(nonwetting_drainage_kr(2:end,1)-nonwetting_drainage_kr(1:end-1,1));
            dkrg_Sg_value = [dkrg_Sg_value(1);dkrg_Sg_value];
            obj.dkrg_Sg_table = griddedInterpolant(nonwetting_drainage_kr(:,1),dkrg_Sg_value);
            dkro_Sg_value = (wetting_kr(2:end,2)-wetting_kr(1:end-1,2))./(wetting_kr(2:end,1)-wetting_kr(1:end-1,1));
            dkro_Sg_value = [dkro_Sg_value;dkro_Sg_value(end)];
            obj.dkro_Sg_table = griddedInterpolant(wetting_kr(:,1),dkro_Sg_value);        
            
            %% capillary pressure
            % convert relations to functions of S_g;
            Pc_drainage(:,1) = 1.0 - Pc_drainage(:,1);
            Pc_imbibation(:,1) = 1.0 - Pc_imbibation(:,1);
            Pc_imbibation = flip(Pc_imbibation); % stored in ascending order
            Pc_drainage = flip(Pc_drainage); % stored in ascending order
            
            drainage_dPc_value = (Pc_drainage(2:end,2)-Pc_drainage(1:end-1,2))./(Pc_drainage(2:end,1)-Pc_drainage(1:end-1,1));
            drainage_dPc_value = [drainage_dPc_value(1);drainage_dPc_value];

            obj.Pc_drainage_Sg_table = griddedInterpolant(Pc_drainage(:,1),Pc_drainage(:,2));
            obj.dPc_drainage_Sg_table = griddedInterpolant(Pc_drainage(:,1),drainage_dPc_value);
            obj.Pc_imbibation_Sg_table = griddedInterpolant(Pc_imbibation(:,1),Pc_imbibation(:,2));
            
        end
        
        function kr = ComputeRelPerm(obj, Phases, Medium)
            status_S_1 = Medium.State.Properties('S_1');
            S = Medium.State.Properties('S_1').Value;
            % safe-guard
            S = max(S, Phases(1).sr);
            S = min(S, 1-Phases(2).sr);
%             % phase 1 (gas phase) 
%             kr(:,1) = obj.krg_Sg_table(S);
%             % phase 2 (oil phase) 
%             kr(:,2) = obj.kro_Sg_table(S);

            % phase 1 (gas phase) 
            kr(:,1) = S/(1-Phases(2).sr);
            % phase 2 (oil phase) 
            kr(:,2) = 1.0 - S/(1-Phases(2).sr);           
  
            status_S_1.Value = S; 
            status_S_2 = Medium.State.Properties('S_2');
            status_S_2.Value = 1.0-S;
            
        end
        
        function dkr = ComputeDerivative(obj, Phases, S)
            % safe-guard
            S = max(S, Phases(1).sr);
            S = min(S, 1-Phases(2).sr);
%             % phase 1 (gas phase) 
%             dkr(:,1) = obj.dkrg_Sg_table(S);
%             % phase 2 (oil phase) 
%             dkr(:,2) = obj.dkro_Sg_table(S);
            
            % phase 1 (gas phase)
            dkr(:,1) = zeros(length(S),1) + 1/(1-Phases(2).sr);
            % phase 2 (oil phase) 
            dkr(:,2) = zeros(length(S),1) - 1/(1-Phases(2).sr);
        end 
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function kr = ComputeRelPermHysteresis(obj, Phases, Reservoir) % dkr is also computted in this function
            status_S_1 = Reservoir.State.Properties('S_1');
            S = Reservoir.State.Properties('S_1').Value;
            % S_old = Reservoir.State_old.Properties('S_1').Value;

            % safe-guard
            S = max(S, Phases(1).sr); 
            S = min(S, 1-Phases(2).sr);
            
            % phase 2 (oil phase, no hysteresis) 
            obj.kr(:,2) = obj.kro_Sg_table(S);
            obj.dkr(:,2) = obj.dkro_Sg_table(S); % dkr

            % phase 1 (gas phase)       
            % no hysteresis
%             obj.kr(:,1) = obj.krg_Sg_table(S);
%             obj.dkr(:,1) = obj.dkrg_Sg_table(S); % dkr

                       
            % with hysteresis
            for i = 1:length(S) % loop each cell
                if (obj.process_label(i)==0)
                    obj.kr(i,1) = obj.krg_Sg_table(S(i));
                    obj.dkr(i,1) = obj.dkrg_Sg_table(S(i)); % dkr
                    
%                     obj.Pc(i) =  obj.drainage_Pc_table(S(i));
%                     obj.dPc(i) = obj.drainage_dPc_table(S(i)); 
                else
                    S(i) = max(S(i),obj.Sg_r(i)); % safe-guard
                    S(i) = min(S(i), obj.reverse_point(i));
                    
                    scanning_curve_kr_table = griddedInterpolant(obj.scanning_curve_Sg(i,:),obj.scanning_curve_kr(i,:));
                    scanning_curve_dkr_table = griddedInterpolant(obj.scanning_curve_Sg(i,:),obj.scanning_curve_dkr(i,:));
                    obj.kr(i,1) = scanning_curve_kr_table(S(i));
                    obj.dkr(i,1) = scanning_curve_dkr_table(S(i)); % dkr
                    
%                     scanning_curve_Pc_table = griddedInterpolant(obj.scanning_curve_Sg(i,:),obj.scanning_curve_Pc(i,:));
%                     scanning_curve_dPc_table = griddedInterpolant(obj.scanning_curve_Sg(i,:),obj.scanning_curve_dPc(i,:));
%                     
%                     obj.Pc(i) = scanning_curve_Pc_table(S(i));
%                     obj.dPc(i) = scanning_curve_dPc_table(S(i));
                end 
            end
            
            kr = obj.kr;
            
            % update saturation and pressure
            
            % obj.safeguard_S_label(:) = 0; % reset label
            % obj.safeguard_S_label(status_S_1.Value ~= S) = 1;
            % status_safeguard_label = Reservoir.State.Properties('safeguard_label');
            % status_safeguard_label.Value = obj.safeguard_S_label;
            
            status_S_1.Value = S; 
            status_S_2 = Reservoir.State.Properties('S_2');
            status_S_2.Value = 1.0-S;
            
            obj.immobile_label = 0;
            obj.immobile_label = (obj.process_label == 1).*(S == obj.Sg_r);
            status_immobile_label = Reservoir.State.Properties('immobile_label');
            status_immobile_label.Value = obj.immobile_label;
            
%             status_z_1 = Reservoir.State.Properties('z_1');
%             status_z_2 = Reservoir.State.Properties('z_2');
%                  
%             obj.rho_v = Reservoir.State.Properties('rho_1').Value;
%             obj.rho_l = Reservoir.State.Properties('rho_2').Value;
%             obj.x_g_v = Reservoir.State.Properties('x_1ph1').Value;
%             obj.x_g_l = Reservoir.State.Properties('x_1ph2').Value;
%             zg = (obj.x_g_l.*obj.rho_l.*(1.0-S) + obj.x_g_v.*obj.rho_v.*S)./(obj.rho_l.*(1.0-S)+obj.rho_v.*S); % re-evaluate zg
%             
%             status_z_1.Value = zg;
%             status_z_2.Value = 1.0 - zg;
        end        
 
        function dkr = ComputeDerivativeHysteresis(obj)
            dkr = obj.dkr;
        end
        
        function ComputeScanningCurve(obj, index)
            %% relative permeability
            S_gi = obj.reverse_point(index);
            
            % compute residual saturation corresponding to S_gi
            % S_gt = obj.S_gcr + (S_gi-obj.S_gcr)/(1+obj.C*(S_gi-obj.S_gcr)); % Land's model
            S_gt = S_gi*0.5; % linear model
            
            obj.Sg_r(index) = S_gt; % update residual saturation

            S_g = linspace(S_gt,S_gi,20)';
            S_gstar = obj.S_gtmax + (S_g-S_gt).*(obj.S_gimax-obj.S_gtmax)/(S_gi-S_gt);
            k_rgi = obj.nonwetting_imbibation_table(S_gstar)*obj.krg_Sg_table(S_gi)/(obj.krg_Sg_table(obj.S_gimax)); % Killough's model           
            dk_rgi = (k_rgi(2:end)-k_rgi(1:end-1))./(S_g(2:end)-S_g(1:end-1)); 
            dk_rgi = [dk_rgi;dk_rgi(end)];
            
            obj.scanning_curve_Sg(index,:) = S_g';
            obj.scanning_curve_kr(index,:) = k_rgi';
            obj.scanning_curve_dkr(index,:) = dk_rgi';
            
            %% capillary pressure
            epsilon = 0.1;
            F = (1./(1.0-S_g-(1.0-S_gi)+epsilon)-1.0/epsilon)./(1./((1.0-S_gt)-(1.0-S_gi)+epsilon)-1.0/epsilon); 
            P_c = obj.Pc_drainage_Sg_table(S_g) + F.*(obj.Pc_imbibation_Sg_table(S_g)-obj.Pc_drainage_Sg_table(S_g)); % Killough's model
            dP_c = (P_c(2:end)-P_c(1:end-1))./(S_g(2:end)-S_g(1:end-1));
            dP_c = [dP_c(1);dP_c];
            
            obj.scanning_curve_Pc(index,:) = P_c';
            obj.scanning_curve_dPc(index,:) = dP_c';
            
        end
        
        function ComputeTransportLabel(obj, Phases, Reservoir)
            N = length(Reservoir.Por);
            % label changes of gas phase mass caused by transport only
            % If gas phase mass increases, transport_label=1, otherwise transport_label=0
%             obj.transport_label(:) = 1; % reset label! (default is drainage)
%             rho_g_STC = 1.98;
%             pv = Reservoir.TotalPV/N; % pore volume of each cell
%             dGas_total = pv.*(Reservoir.State_old.Properties('S_1').Value.*Reservoir.State_old.Properties('rho_1').Value - Reservoir.State_old_old.Properties('S_1').Value.*Reservoir.State_old_old.Properties('rho_1').Value);
%             dGas_dissolution = rho_g_STC*pv.*(Reservoir.State_old.Properties('S_2').Value.*Reservoir.State_old.Properties('Rs').Value - Reservoir.State_old_old.Properties('S_2').Value.*Reservoir.State_old_old.Properties('Rs').Value);
%             obj.transport_label((dGas_total+dGas_dissolution)<0) = 0;
            
            S_old = Reservoir.State_old.Properties('S_1').Value;
            S_old_old = Reservoir.State_old_old.Properties('S_1').Value;
            
            for i = 1:N % loop each cell
                % if (obj.process_label(i)==0 && obj.transport_label(i)==0 && (S_old(i) < S_old_old(i)))
                if (obj.process_label(i)==0 && S_old(i) < S_old_old(i) && abs(S_old(i)-S_old_old(i))>1.0e-4)    
                    S_gi = S_old_old(i);
                    % compute residual saturation corresponding to S_gi
                    % S_gt = obj.S_gcr + (S_gi-obj.S_gcr)/(1+obj.C*(S_gi-obj.S_gcr)); 
                    S_gt = S_gi*0.5; % linear model
                    if (abs(S_gt-S_gi)>1.0e-3) %1.0e-3
                        obj.process_label(i)=1;
                        obj.reverse_point(i) = S_old_old(i); % update reverse point 
                        obj.ComputeScanningCurve(i);
                    end

                else if (obj.process_label(i)==1 && (S_old(i) > S_old_old(i)) && S_old(i)==obj.reverse_point(i))
                        obj.process_label(i)=0;
                        obj.reverse_point(i) = 1-Phases(2).sr; % update reverse point
                        obj.Sg_r(i) = Phases(1).sr; % update residual saturation
                    end
                end
            end  
            status_process_label = Reservoir.State.Properties('process_label'); % update process_label
            status_process_label.Value = obj.process_label;
            status_reverse_point = Reservoir.State.Properties('reverse_point'); % update reverse_point
            status_reverse_point.Value = obj.reverse_point;
            status_residual_saturation = Reservoir.State.Properties('residual_saturation'); % update residual saturation
            status_residual_saturation.Value = obj.Sg_r;
        end
    
        % reset relative permeability model
        function ResetRelPermModel(obj, State)
            obj.process_label = State.Properties('process_label').Value;
            obj.reverse_point = State.Properties('reverse_point').Value;
            obj.Sg_r = State.Properties('residual_saturation').Value;
        end
              
    end
end