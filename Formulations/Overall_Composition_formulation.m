% Overall Composition Formulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DARSim 2 Reservoir Simulator
%Author: Matteo Cusini, Yuhang Wang
%TU Delft
%Created: 13 December 2017
%Last modified: 26 July 2021
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
classdef Overall_Composition_formulation < Compositional_formulation
    properties
        dxdz
        dxdp
        drhoTdp
        drhodz
        drhoTdz
        dMobdp
        dMobdz
        
        rho_old
        rho
        P
        z_old
        z 
        x
        
        Injection_time % well shutdown control
    end
    methods
        function obj = Overall_Composition_formulation(SimulationInput,n_phases,n_components)
            obj@Compositional_formulation(n_components);
            
            % allocate memory
            if (SimulationInput.FracturesProperties.isFractured)
                N = SimulationInput.ReservoirProperties.Grid.N_ActiveCells + SimulationInput.FracturesProperties.NumOfAllFracGrids; % total number of cells
            else
                N = SimulationInput.ReservoirProperties.Grid.N_ActiveCells;
            end

            obj.rho_old = zeros(N, n_phases);
            obj.rho = zeros(N, n_phases);
            obj.P = zeros(N, n_phases);
            obj.z_old = zeros(N, n_components);
            obj.z = zeros(N, n_components); 
            obj.x = zeros(N, n_components*n_phases);
            
            obj.Injection_time = 3600*24*600; % -1
        end
        %%
        function x = GetPrimaryUnknowns(obj, ProductionSystem, DiscretizationModel)
            Nt = DiscretizationModel.N;
            Nm = DiscretizationModel.ReservoirGrid.N;
            x = zeros(obj.NofComponents * Nt, 1);
            %% 1. reservoir
            Start = 1;
            End = Nm;
            x(Start:End) = ProductionSystem.Reservoir.State.Properties('P_2').Value;
            for i=1:obj.NofComponents-1
                Start = End + 1;
                End = Start + Nm - 1;
                x(Start:End) = ProductionSystem.Reservoir.State.Properties(['z_', num2str(i)]).Value;
            end            
            %% 2. fracture
            if ProductionSystem.FracturesNetwork.Active
                Nf = DiscretizationModel.FracturesGrid.N;
                for f = 1:ProductionSystem.FracturesNetwork.NumOfFrac
                    Start = End + 1;
                    End = Start + Nf(f) - 1;
                    x(Start:End) = ProductionSystem.FracturesNetwork.Fractures(f).State.Properties('P_2').Value;
                    for i=1:obj.NofComponents-1
                        Start = End + 1;
                        End = Start + Nf(f) - 1;
                        x(Start:End) = ProductionSystem.FracturesNetwork.Fractures(f).State.Properties(['z_', num2str(i)]).Value;
                    end
                end
            end           
        end
        %%
        function ComputePropertiesAndDerivatives(obj, ProductionSystem, FluidModel, ~)
            %% 1. reservoir
            % This is the bitchy part! (Indeed)
            [obj.dxdp, obj.dxdz] = FluidModel.DxDpDz(ProductionSystem.Reservoir.State, ProductionSystem.Reservoir.State.Properties('SinglePhase').Value);
            % %%%%%%%%%%%%%%%%%%%%%%%%
            obj.drhodp = FluidModel.ComputeDrhoDp(ProductionSystem.Reservoir.State, ProductionSystem.Reservoir.State.Properties('SinglePhase').Value);
            obj.drhodz = FluidModel.ComputeDrhoDz(ProductionSystem.Reservoir.State, ProductionSystem.Reservoir.State.Properties('SinglePhase').Value);
            dSdp = FluidModel.ComputeDSDp(ProductionSystem.Reservoir.State, obj.drhodp, -obj.dxdp(:,5));
            dSdz = FluidModel.ComputeDSDz(ProductionSystem.Reservoir.State, -obj.dxdz(:, end));
            obj.drhoTdz = FluidModel.ComputeDrhotDz(ProductionSystem.Reservoir.State, obj.drhodz, dSdz);
            obj.drhoTdp = FluidModel.ComputeDrhotDp(ProductionSystem.Reservoir.State,obj.drhodp, dSdp);
            if FluidModel.HysteresisActive 
                obj.dMobdp = FluidModel.ComputeDMobDpHysteresis(ProductionSystem.Reservoir.State, [dSdp, dSdp]);
                obj.dMob = FluidModel.ComputeDMobDzHysteresis(ProductionSystem.Reservoir.State, dSdz);      
            else
                obj.dMobdp = FluidModel.ComputeDMobDp(ProductionSystem.Reservoir.State, [dSdp, dSdp]); % I am not using it !
                obj.dMob = FluidModel.ComputeDMobDz(ProductionSystem.Reservoir.State, dSdz);
            end
            obj.dPc = FluidModel.ComputeDPcDz(ProductionSystem.Reservoir.State, dSdz, 1);            
            %% 2. fracture       
            for f = 1:ProductionSystem.FracturesNetwork.NumOfFrac
                
                [dxdp_temp, dxdz_temp] = FluidModel.DxDpDz(ProductionSystem.FracturesNetwork.Fractures(f).State, ProductionSystem.FracturesNetwork.Fractures(f).State.Properties('SinglePhase').Value);

                % %%%%%%%%%%%%%%%%%%%%%%%%
                drhodp_temp = FluidModel.ComputeDrhoDp(ProductionSystem.FracturesNetwork.Fractures(f).State, ProductionSystem.FracturesNetwork.Fractures(f).State.Properties('SinglePhase').Value);
                drhodz_temp = FluidModel.ComputeDrhoDz(ProductionSystem.FracturesNetwork.Fractures(f).State, ProductionSystem.FracturesNetwork.Fractures(f).State.Properties('SinglePhase').Value);
                dSdp = FluidModel.ComputeDSDp(ProductionSystem.FracturesNetwork.Fractures(f).State, drhodp_temp, -dxdp_temp(:,5));
                dSdz = FluidModel.ComputeDSDz(ProductionSystem.FracturesNetwork.Fractures(f).State, -dxdz_temp(:, end));

                obj.drhoTdz = [obj.drhoTdz; FluidModel.ComputeDrhotDz(ProductionSystem.FracturesNetwork.Fractures(f).State, drhodz_temp, dSdz)];
                obj.drhoTdp = [obj.drhoTdp; FluidModel.ComputeDrhotDp(ProductionSystem.FracturesNetwork.Fractures(f).State, drhodp_temp, dSdp)];
                obj.dMobdp = [obj.dMobdp; FluidModel.ComputeDMobDp(ProductionSystem.FracturesNetwork.Fractures(f).State, [dSdp, dSdp])];
                obj.dMob = [obj.dMob; FluidModel.ComputeDMobDz(ProductionSystem.FracturesNetwork.Fractures(f).State, dSdz)];
                obj.dPc = [obj.dPc; FluidModel.ComputeDPcDz(ProductionSystem.FracturesNetwork.Fractures(f).State, dSdz, 0)];
                
                obj.dxdp = [obj.dxdp; dxdp_temp];
                obj.dxdz = [obj.dxdz; dxdz_temp];
                obj.drhodp = [obj.drhodp; drhodp_temp];
                obj.drhodz = [obj.drhodz; drhodz_temp];
            end
            
        end
        %%
        function [Residual, RHS] = BuildMediumResidual(obj, Medium, Grid, dt, State0, Index, qw, qf, f, c)
            N = Grid.N;
            pv = Medium.Por .* Grid.Volume;

            rhoT_old = State0.Properties('rhoT').Value(Index.Start:Index.End);
            rhoT =  Medium.State.Properties('rhoT').Value;
            
            % Accumulation term
            n = rhoT .* obj.z(Index.Start:Index.End,c);
            n_old = rhoT_old .* obj.z_old(Index.Start:Index.End,c);

            A = speye(N).*pv/dt;
            
            % Residual  
            RHS = qw(Index.Start:Index.End, c);
%             Residual = A * (n - n_old) ... % accumulation
%                      + obj.Tph{c, 1} *  obj.P(:,1) ... % Convective term                
%                      + obj.Tph{c, 2} *  obj.P(:,2) ...
%                      - obj.Gph{c,1} * Grid.Depth ... % Gravity
%                      - obj.Gph{c,2} * Grid.Depth ...
%                      - qw(Index.Start:Index.End,c); % Source/Sink 

            Residual = A * (n - n_old) ... % accumulation
                     + obj.Tph{2*c-1, 1+f} *  obj.P(Index.Start:Index.End,1) ... % Convective term                
                     + obj.Tph{2*c, 1+f} *  obj.P(Index.Start:Index.End,2) ...
                     - obj.Gph{2*c-1,1+f} * Grid.Depth ... % Gravity
                     - obj.Gph{2*c,1+f} * Grid.Depth ...
                     - qw(Index.Start:Index.End, c) ... % Source/Sink 
                     - qf(Index.Start:Index.End, c); % frac-matrix
            
        end
        %%
        function CopytoLocalVariables(obj, ProductionSystem, DiscretizationModel, State0)
            Index.Start = 1;
            Index.End = DiscretizationModel.ReservoirGrid.N;
            % Reservoir
            % Copy values to local variables (phase-dependent)
            for i = 1:obj.NofPhases
                obj.rho_old(Index.Start:Index.End, i) = State0.Properties(['rho_', num2str(i)]).Value(Index.Start:Index.End);
                obj.P(Index.Start:Index.End, i) = ProductionSystem.Reservoir.State.Properties(['P_', num2str(i)]).Value;
                obj.rho(Index.Start:Index.End, i) = ProductionSystem.Reservoir.State.Properties(['rho_', num2str(i)]).Value;
            end
            % Copy values to local variables (component-dependent)
            for i=1:obj.NofComponents
                obj.z_old(Index.Start:Index.End,i) = State0.Properties(['z_', num2str(i)]).Value(Index.Start:Index.End);
                obj.z(Index.Start:Index.End, i) = ProductionSystem.Reservoir.State.Properties(['z_', num2str(i)]).Value;
                for j=1:obj.NofPhases
                    obj.x(Index.Start:Index.End,(i-1)*obj.NofPhases + j) = ProductionSystem.Reservoir.State.Properties(['x_', num2str(i),'ph',num2str(j)]).Value;
                end
            end
            % Fractures
            for f = 1:ProductionSystem.FracturesNetwork.NumOfFrac
                Index.Start = Index.End+1;
                Index.End = Index.Start + DiscretizationModel.FracturesGrid.N(f) - 1;
                % Copy values to local variables (phase-dependent)
                for i = 1:obj.NofPhases
                    obj.rho_old(Index.Start:Index.End, i) = State0.Properties(['rho_', num2str(i)]).Value(Index.Start:Index.End);
                    obj.P(Index.Start:Index.End, i) = ProductionSystem.FracturesNetwork.Fractures(f).State.Properties(['P_', num2str(i)]).Value;
                    obj.rho(Index.Start:Index.End, i) = ProductionSystem.FracturesNetwork.Fractures(f).State.Properties(['rho_', num2str(i)]).Value;
                end
                % Copy values to local variables (component-dependent)
                for i=1:obj.NofComponents
                    obj.z_old(Index.Start:Index.End,i) = State0.Properties(['z_', num2str(i)]).Value(Index.Start:Index.End);
                    obj.z(Index.Start:Index.End, i) = ProductionSystem.FracturesNetwork.Fractures(f).State.Properties(['z_', num2str(i)]).Value;
                    for j=1:obj.NofPhases
                        obj.x(Index.Start:Index.End,(i-1)*obj.NofPhases + j) = ProductionSystem.FracturesNetwork.Fractures(f).State.Properties(['x_', num2str(i),'ph',num2str(j)]).Value;
                    end
                end
            end 
        end
        %%
        function [Residual, RHS] = BuildFullResidual(obj, ProductionSystem, DiscretizationModel, Time, dt, State0)
            % Create local variables
%             N = DiscretizationModel.ReservoirGrid.N;          
%             pv = ProductionSystem.Reservoir.Por*DiscretizationModel.ReservoirGrid.Volume;
            
%             z_old = zeros(N, obj.NofComponents);
%             rho_old = zeros(N, obj.NofPhases);
%             P = zeros(N, obj.NofPhases);
%             z = zeros(N, obj.NofComponents);
%             rho = zeros(N, obj.NofPhases);
%             x = zeros(N, obj.NofComponents*obj.NofPhases);
            
            % Copy values in local variables
%             for i=1:obj.NofPhases
%                 obj.rho_old(:,i) = State0.Properties(['rho_', num2str(i)]).Value;
%                 obj.P(:, i) = ProductionSystem.Reservoir.State.Properties(['P_', num2str(i)]).Value;
%                 obj.rho(:, i) = ProductionSystem.Reservoir.State.Properties(['rho_', num2str(i)]).Value;
%             end
%             for i=1:obj.NofComponents
%                 obj.z_old(:,i) = State0.Properties(['z_', num2str(i)]).Value;
%                 obj.z(:, i) = ProductionSystem.Reservoir.State.Properties(['z_', num2str(i)]).Value;
%                 for j=1:obj.NofPhases
%                     obj.x(:,(i-1)*obj.NofPhases + j) = ProductionSystem.Reservoir.State.Properties(['x_', num2str(i),'ph',num2str(j)]).Value;
%                 end
%             end
%             rhoT_old = State0.Properties('rhoT').Value;
%             rhoT =  ProductionSystem.Reservoir.State.Properties('rhoT').Value;
%             
%             % Depths
%             depth = DiscretizationModel.ReservoirG2rid.Depth;
            
            % Accumulation term
            % A = speye(N).*pv/dt;
            
            % Source terms
%             if (Time <= obj.Injection_time) % well shutdown control
%                 q = obj.ComputeSourceTerms(N, ProductionSystem.Wells);
%             else
%                 q = zeros(N,2);
%             end
            
            % Build Residual
%             Residual = zeros(N*obj.NofComponents, 1);
%             for i=1:obj.NofComponents
%                 % Total moles
%                 n = rhoT .* obj.z(:,i);
%                 n_old = rhoT_old .* obj.z_old(:,i);
%                 % Phase Transmissibilities
%                 obj.TransmissibilityMatrix(DiscretizationModel.ReservoirGrid, obj.rho, obj.GravityModel.RhoInt, obj.x(:,(i-1)*2+1:(i-1)*2+2), i); 
%                 RHS((i-1)*N+1:i*N) = q(:,i);
%                 Residual((i-1)*N+1:i*N) = A * (n - n_old) ...       % Accumulation term
%                            + obj.Tph{i, 1} *  obj.P(:,1) ...       % Convective term                
%                            + obj.Tph{i, 2} *  obj.P(:,2)...
%                            - obj.Gph{i,1} * depth...       % Gravity
%                            - obj.Gph{i,2} * depth...
%                            - q(:,i);                       % Source/Sink  
%             end       

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Initialise residual vector
            Nt = DiscretizationModel.N;
            Residual = zeros(Nt * obj.NofComponents, 1);
            RHS = zeros(Nt * obj.NofComponents, 1);
            Nm = DiscretizationModel.ReservoirGrid.N;

            % Copy values to local variables
            CopytoLocalVariables(obj, ProductionSystem, DiscretizationModel, State0);
            
            % Source terms
            % well
            if (Time <= obj.Injection_time) % well shutdown control
                qw = obj.ComputeSourceTerms(Nt, ProductionSystem.Wells);
            else
                qw = zeros(Nt,2);
            end
            % fracture_matrix
            qf = zeros(Nt, obj.NofComponents);
            if ProductionSystem.FracturesNetwork.Active
                qf = ComputeSourceTerms_frac_mat(obj, ProductionSystem, DiscretizationModel);
            end
 
            % Build residual
            for c = 1:obj.NofComponents 
                % Reservoir
                % Index
                Index.Start = 1;
                Index.End = Nm;
                % Phase Transmissibilities
                obj.TransmissibilityMatrix(DiscretizationModel.ReservoirGrid, obj.rho(Index.Start:Index.End,:), obj.x(Index.Start:Index.End,(c-1)*2+1:(c-1)*2+2), Index, c, 0); 
                % Residual
                [Residualm, RHSm] = BuildMediumResidual(obj, ProductionSystem.Reservoir, DiscretizationModel.ReservoirGrid, dt, State0, Index, qw, qf, 0, c);
                Residual((c-1)*Nt + Index.Start: (c-1)*Nt + Index.End) = Residualm;
                RHS(     (c-1)*Nt + Index.Start: (c-1)*Nt + Index.End) = RHSm; 
                
                % Fractures
                for f = 1:ProductionSystem.FracturesNetwork.NumOfFrac
                    % Index
                    Index.Start = Index.End+1;
                    Index.End = Index.Start + DiscretizationModel.FracturesGrid.N(f) - 1;
                    % Phase Transmissibilities
                    obj.TransmissibilityMatrix(DiscretizationModel.FracturesGrid.Grids(f), obj.rho(Index.Start:Index.End,:), obj.x(Index.Start:Index.End,(c-1)*2+1:(c-1)*2+2), Index, c, f);
                    % Residual
                    Residual_frac_f = BuildMediumResidual(obj, ProductionSystem.FracturesNetwork.Fractures(f), DiscretizationModel.FracturesGrid.Grids(f), dt, State0, Index, qw, qf, f, c);
                    Residual((c-1)*Nt + Index.Start: (c-1)*Nt + Index.End) = Residual_frac_f;
                end
            end
        end
        %%
        function [Jp, Jz] = BuildMediumJacobian(obj, Medium, Grid, dt, Index, f, c)
            % Create local variables
            Nx = Grid.Nx;
            Ny = Grid.Ny;
            Nz = Grid.Nz;
            N = Grid.N;
            pv = Grid.Volume .* Medium.Por;
            
            rhoT = Medium.State.Properties('rhoT').Value;
            
            % 1.a: divergence
            Jp = obj.Tph{2*c-1,1+f}  + obj.Tph{2*c, 1+f};
            
            % 1.b: compressibility part
            dMupxPh1 = obj.UpWind{1,1+f}.x * (obj.Mob(Index.Start:Index.End, 1) .* obj.x(Index.Start:Index.End,(c-1)*2+1) .* obj.drhodp(Index.Start:Index.End,1) + obj.Mob(Index.Start:Index.End, 1) .* obj.dxdp(Index.Start:Index.End,(c-1)*2+1) .* obj.rho(Index.Start:Index.End,1) + obj.dMobdp(Index.Start:Index.End, 1) .* obj.x(Index.Start:Index.End,(c-1)*2+1) .* obj.rho(Index.Start:Index.End,1));
            dMupyPh1 = obj.UpWind{1,1+f}.y * (obj.Mob(Index.Start:Index.End, 1) .* obj.x(Index.Start:Index.End,(c-1)*2+1) .* obj.drhodp(Index.Start:Index.End,1) + obj.Mob(Index.Start:Index.End, 1) .* obj.dxdp(Index.Start:Index.End,(c-1)*2+1) .* obj.rho(Index.Start:Index.End,1) + obj.dMobdp(Index.Start:Index.End, 1) .* obj.x(Index.Start:Index.End,(c-1)*2+1) .* obj.rho(Index.Start:Index.End,1));
            dMupzPh1 = obj.UpWind{1,1+f}.z * (obj.Mob(Index.Start:Index.End, 1) .* obj.x(Index.Start:Index.End,(c-1)*2+1) .* obj.drhodp(Index.Start:Index.End,1) + obj.Mob(Index.Start:Index.End, 1) .* obj.dxdp(Index.Start:Index.End,(c-1)*2+1) .* obj.rho(Index.Start:Index.End,1) + obj.dMobdp(Index.Start:Index.End, 1) .* obj.x(Index.Start:Index.End,(c-1)*2+1) .* obj.rho(Index.Start:Index.End,1));
            dMupxPh2 = obj.UpWind{2,1+f}.x * (obj.Mob(Index.Start:Index.End, 2) .* obj.x(Index.Start:Index.End,(c-1)*2+2) .* obj.drhodp(Index.Start:Index.End,2) + obj.Mob(Index.Start:Index.End, 2) .* obj.dxdp(Index.Start:Index.End,(c-1)*2+2) .* obj.rho(Index.Start:Index.End,2) + obj.dMobdp(Index.Start:Index.End, 2) .* obj.x(Index.Start:Index.End,(c-1)*2+2) .* obj.rho(Index.Start:Index.End,2));
            dMupyPh2 = obj.UpWind{2,1+f}.y * (obj.Mob(Index.Start:Index.End, 2) .* obj.x(Index.Start:Index.End,(c-1)*2+2) .* obj.drhodp(Index.Start:Index.End,2) + obj.Mob(Index.Start:Index.End, 2) .* obj.dxdp(Index.Start:Index.End,(c-1)*2+2) .* obj.rho(Index.Start:Index.End,2) + obj.dMobdp(Index.Start:Index.End, 2) .* obj.x(Index.Start:Index.End,(c-1)*2+2) .* obj.rho(Index.Start:Index.End,2));
            dMupzPh2 = obj.UpWind{2,1+f}.z * (obj.Mob(Index.Start:Index.End, 2) .* obj.x(Index.Start:Index.End,(c-1)*2+2) .* obj.drhodp(Index.Start:Index.End,2) + obj.Mob(Index.Start:Index.End, 2) .* obj.dxdp(Index.Start:Index.End,(c-1)*2+2) .* obj.rho(Index.Start:Index.End,2) + obj.dMobdp(Index.Start:Index.End, 2) .* obj.x(Index.Start:Index.End,(c-1)*2+2) .* obj.rho(Index.Start:Index.End,2));
            
            vecX1 = min(reshape(obj.U{1,1+f}.x(1:Nx,:,:),N,1), 0).*dMupxPh1 + min(reshape(obj.U{2,1+f}.x(1:Nx,:,:),N,1), 0).*dMupxPh2;
            vecX2 = max(reshape(obj.U{1,1+f}.x(2:Nx+1,:,:),N,1), 0).*dMupxPh1 + max(reshape(obj.U{2,1+f}.x(2:Nx+1,:,:),N,1), 0).*dMupxPh2;
            vecY1 = min(reshape(obj.U{1,1+f}.y(:,1:Ny,:),N,1), 0).*dMupyPh1 + min(reshape(obj.U{2,1+f}.y(:,1:Ny,:),N,1), 0).*dMupyPh2;
            vecY2 = max(reshape(obj.U{1,1+f}.y(:,2:Ny+1,:),N,1), 0).*dMupyPh1 + max(reshape(obj.U{2,1+f}.y(:,2:Ny+1,:),N,1), 0).*dMupyPh2;
            vecZ1 = min(reshape(obj.U{1,1+f}.z(:,:,1:Nz),N,1), 0).*dMupzPh1 + min(reshape(obj.U{2,1+f}.z(:,:,1:Nz),N,1), 0).*dMupzPh2;
            vecZ2 = max(reshape(obj.U{1,1+f}.z(:,:,2:Nz+1),N,1), 0).*dMupzPh1 + max(reshape(obj.U{2,1+f}.z(:,:,2:Nz+1),N,1), 0).*dMupzPh2;
            acc = pv/dt .* (obj.drhoTdp(Index.Start:Index.End) .* obj.z(Index.Start:Index.End,c));
            DiagVecs = [-vecZ2, -vecY2, -vecX2, vecZ2 + vecY2+vecX2-vecY1-vecX1-vecZ1+acc, vecX1, vecY1, vecZ1];
            DiagIndx = [-Nx*Ny, -Nx, -1, 0, 1, Nx, Nx*Ny];
            Jp = Jp + spdiags(DiagVecs, DiagIndx, N, N);
                     
            % 2. Component i composition block
            for j=1:obj.NofComponents-1 
                dMupxPh1 = obj.UpWind{1,1+f}.x*...
                    ( obj.dMob(Index.Start:Index.End, 1, j) .* obj.x(Index.Start:Index.End,(c-1)*2+1) .* obj.rho(Index.Start:Index.End,1)...
                    + obj.Mob(Index.Start:Index.End, 1) .* obj.dxdz(Index.Start:Index.End,(c-1)*2+1, j) .* obj.rho(Index.Start:Index.End,1)...
                    + obj.Mob(Index.Start:Index.End, 1) .* obj.x(Index.Start:Index.End, (c-1)*2+1) .* obj.drhodz(Index.Start:Index.End, 1));
                dMupyPh1 = obj.UpWind{1,1+f}.y*...
                    ( obj.dMob(Index.Start:Index.End, 1, j) .* obj.x(Index.Start:Index.End, (c-1)*2+1) .* obj.rho(Index.Start:Index.End,1)...
                    + obj.Mob(Index.Start:Index.End, 1) .* obj.dxdz(Index.Start:Index.End, (c-1)*2+1, j) .* obj.rho(Index.Start:Index.End,1)...
                    + obj.Mob(Index.Start:Index.End, 1) .* obj.x(Index.Start:Index.End,(c-1)*2+1) .* obj.drhodz(Index.Start:Index.End, 1));
                dMupzPh1 = obj.UpWind{1,1+f}.z*...
                    ( obj.dMob(Index.Start:Index.End, 1, j) .* obj.x(Index.Start:Index.End,(c-1)*2+1) .* obj.rho(Index.Start:Index.End,1)...
                    + obj.Mob(Index.Start:Index.End,1) .* obj.dxdz(Index.Start:Index.End,(c-1)*2+1, j) .* obj.rho(Index.Start:Index.End,1)...
                    + obj.Mob(Index.Start:Index.End,1) .* obj.x(Index.Start:Index.End,(c-1)*2+1) .* obj.drhodz(Index.Start:Index.End, 1));
                dMupxPh2 = obj.UpWind{2,1+f}.x*...
                    ( obj.dMob(Index.Start:Index.End, 2, j) .* obj.x(Index.Start:Index.End,(c-1)*2+2) .* obj.rho(Index.Start:Index.End,2)...
                    + obj.Mob(Index.Start:Index.End,2) .* obj.dxdz(Index.Start:Index.End,(c-1)*2+2, j) .* obj.rho(Index.Start:Index.End,2)...
                    + obj.Mob(Index.Start:Index.End,2) .* obj.x(Index.Start:Index.End,(c-1)*2+2) .* obj.drhodz(Index.Start:Index.End, 2));
                dMupyPh2 = obj.UpWind{2,1+f}.y*...
                    ( obj.dMob(Index.Start:Index.End, 2, j) .* obj.x(Index.Start:Index.End,(c-1)*2+2) .* obj.rho(Index.Start:Index.End,2)...
                    + obj.Mob(Index.Start:Index.End, 2) .* obj.dxdz(Index.Start:Index.End,(c-1)*2+2, j) .* obj.rho(Index.Start:Index.End,2)...
                    + obj.Mob(Index.Start:Index.End, 2) .* obj.x(Index.Start:Index.End, (c-1)*2+2) .* obj.drhodz(Index.Start:Index.End, 2));
                dMupzPh2 = obj.UpWind{2,1+f}.z*...
                    ( obj.dMob(Index.Start:Index.End, 2, j) .* obj.x(Index.Start:Index.End, (c-1)*2+2) .* obj.rho(Index.Start:Index.End,2)...
                    + obj.Mob(Index.Start:Index.End,2) .* obj.dxdz(Index.Start:Index.End, (c-1)*2+2, j) .* obj.rho(Index.Start:Index.End,2)...
                    + obj.Mob(Index.Start:Index.End,2) .* obj.x(Index.Start:Index.End, (c-1)*2+2) .* obj.drhodz(Index.Start:Index.End, 2));
                
                vecX1 = min(reshape(obj.U{1,1+f}.x(1:Nx,:,:),N,1), 0).*dMupxPh1 + min(reshape(obj.U{2,1+f}.x(1:Nx,:,:),N,1), 0).*dMupxPh2;
                vecX2 = max(reshape(obj.U{1,1+f}.x(2:Nx+1,:,:),N,1), 0).*dMupxPh1 + max(reshape(obj.U{2,1+f}.x(2:Nx+1,:,:),N,1), 0).*dMupxPh2;
                vecY1 = min(reshape(obj.U{1,1+f}.y(:,1:Ny,:),N,1), 0).*dMupyPh1 + min(reshape(obj.U{2,1+f}.y(:,1:Ny,:),N,1), 0).*dMupyPh2;
                vecY2 = max(reshape(obj.U{1,1+f}.y(:,2:Ny+1,:),N,1), 0).*dMupyPh1 + max(reshape(obj.U{2,1+f}.y(:,2:Ny+1,:),N,1), 0).*dMupyPh2;
                vecZ1 = min(reshape(obj.U{1,1+f}.z(:,:,1:Nz),N,1), 0).*dMupzPh1 + min(reshape(obj.U{2,1+f}.z(:,:,1:Nz),N,1), 0).*dMupzPh2;
                vecZ2 = max(reshape(obj.U{1,1+f}.z(:,:,2:Nz+1),N,1), 0).*dMupzPh1 + max(reshape(obj.U{2,1+f}.z(:,:,2:Nz+1),N,1), 0).*dMupzPh2;
                
                if c == obj.NofComponents
                    acc = - pv/dt .* rhoT + pv/dt .* obj.z(Index.Start:Index.End,c) .* obj.drhoTdz(Index.Start:Index.End, j);
                elseif c == j
                    acc = pv/dt .* rhoT + pv/dt .* obj.z(Index.Start:Index.End,c) .* obj.drhoTdz(Index.Start:Index.End, j);
                end
                
                DiagVecs = [-vecZ2, -vecY2, -vecX2, vecZ2+vecY2+vecX2-vecZ1-vecY1-vecX1+acc, vecX1, vecY1, vecZ1];
                DiagIndx = [-Nx*Ny, -Nx, -1, 0, 1, Nx, Nx*Ny];
                Jz = spdiags(DiagVecs,DiagIndx, N, N);
                % Capillarity
                % Jz{i,j} = Jz{i,j} - obj.Tph{i,1} * spdiags(obj.dPc, 0, N, N);
                Jz = Jz - obj.Tph{2*c-1,1+f} * spdiags(obj.dPc(Index.Start:Index.End), 0, N, N);
                
            end  
        end              
        %%
        function Jacobian = BuildFullJacobian(obj, ProductionSystem, DiscretizationModel, Time, dt)
            % BUILD FIM JACOBIAN BLOCK BY BLOCK
            
            % Initialise local variables
%             Nx = DiscretizationModel.ReservoirGrid.Nx;
%             Ny = DiscretizationModel.ReservoirGrid.Ny;
%             Nz = DiscretizationModel.ReservoirGrid.Nz;
%             N = DiscretizationModel.ReservoirGrid.N;
%             %
%             pv = DiscretizationModel.ReservoirGrid.Volume*ProductionSystem.Reservoir.Por;
            
%             z = zeros(N, obj.NofComponents);
%             rho = zeros(N, obj.NofPhases);
%             P = zeros(N, obj.NofPhases);
%             x = zeros(N, obj.NofComponents*obj.NofPhases);
            
            % Copy values in local variables
%             for i=1:obj.NofPhases
%                 obj.P(:, i) = ProductionSystem.Reservoir.State.Properties(['P_', num2str(i)]).Value;
%                 obj.rho(:, i) = ProductionSystem.Reservoir.State.Properties(['rho_', num2str(i)]).Value;
%             end
%             for i=1:obj.NofComponents
%                 obj.z(:, i) = ProductionSystem.Reservoir.State.Properties(['z_', num2str(i)]).Value;
%                 for j=1:obj.NofPhases
%                     obj.x(:,(i-1)*obj.NofPhases + j) = ProductionSystem.Reservoir.State.Properties(['x_', num2str(i),'ph',num2str(j)]).Value;
%                 end
%             end
%             rhoT =  ProductionSystem.Reservoir.State.Properties('rhoT').Value;
            
            % Fill in block by block
%             Jp = cell(obj.NofComponents, 1);
%             Jz = cell(obj.NofComponents, obj.NofComponents-1);
%             for i=1:obj.NofComponents
%                 %% 1. Component i pressure block
%                 % 1.a: divergence
%                 % Jp{i} = obj.Tph{i,1}  + obj.Tph{i, 2};
%                 Jp{i} = obj.Tph{2*i-1,1}  + obj.Tph{2*i, 1};
%                 
% 
%                 % 1.b: compressibility part
%                 dMupxPh1 = obj.UpWind{1}.x * (obj.Mob(:, 1) .* obj.x(:,(i-1)*2+1) .* obj.drhodp(:,1) + obj.Mob(:, 1) .* obj.dxdp(:,(i-1)*2+1) .* obj.rho(:,1) + obj.dMobdp(:, 1) .* obj.x(:,(i-1)*2+1) .* obj.rho(:,1));
%                 dMupyPh1 = obj.UpWind{1}.y * (obj.Mob(:, 1) .* obj.x(:,(i-1)*2+1) .* obj.drhodp(:,1) + obj.Mob(:, 1) .* obj.dxdp(:,(i-1)*2+1) .* obj.rho(:,1) + obj.dMobdp(:, 1) .* obj.x(:,(i-1)*2+1) .* obj.rho(:,1));
%                 dMupzPh1 = obj.UpWind{1}.z * (obj.Mob(:, 1) .* obj.x(:,(i-1)*2+1) .* obj.drhodp(:,1) + obj.Mob(:, 1) .* obj.dxdp(:,(i-1)*2+1) .* obj.rho(:,1) + obj.dMobdp(:, 1) .* obj.x(:,(i-1)*2+1) .* obj.rho(:,1));
%                 dMupxPh2 = obj.UpWind{2}.x * (obj.Mob(:, 2) .* obj.x(:,(i-1)*2+2) .* obj.drhodp(:,2) + obj.Mob(:, 2) .* obj.dxdp(:,(i-1)*2+2) .* obj.rho(:,2) + obj.dMobdp(:, 2) .* obj.x(:,(i-1)*2+2) .* obj.rho(:,2));
%                 dMupyPh2 = obj.UpWind{2}.y * (obj.Mob(:, 2) .* obj.x(:,(i-1)*2+2) .* obj.drhodp(:,2) + obj.Mob(:, 2) .* obj.dxdp(:,(i-1)*2+2) .* obj.rho(:,2) + obj.dMobdp(:, 2) .* obj.x(:,(i-1)*2+2) .* obj.rho(:,2));
%                 dMupzPh2 = obj.UpWind{2}.z * (obj.Mob(:, 2) .* obj.x(:,(i-1)*2+2) .* obj.drhodp(:,2) + obj.Mob(:, 2) .* obj.dxdp(:,(i-1)*2+2) .* obj.rho(:,2) + obj.dMobdp(:, 2) .* obj.x(:,(i-1)*2+2) .* obj.rho(:,2));
%                 
%                 vecX1 = min(reshape(obj.U{1}.x(1:Nx,:,:),N,1), 0).*dMupxPh1 + min(reshape(obj.U{2}.x(1:Nx,:,:),N,1), 0).*dMupxPh2;
%                 vecX2 = max(reshape(obj.U{1}.x(2:Nx+1,:,:),N,1), 0).*dMupxPh1 + max(reshape(obj.U{2}.x(2:Nx+1,:,:),N,1), 0).*dMupxPh2;
%                 vecY1 = min(reshape(obj.U{1}.y(:,1:Ny,:),N,1), 0).*dMupyPh1 + min(reshape(obj.U{2}.y(:,1:Ny,:),N,1), 0).*dMupyPh2;
%                 vecY2 = max(reshape(obj.U{1}.y(:,2:Ny+1,:),N,1), 0).*dMupyPh1 + max(reshape(obj.U{2}.y(:,2:Ny+1,:),N,1), 0).*dMupyPh2;
%                 vecZ1 = min(reshape(obj.U{1}.z(:,:,1:Nz),N,1), 0).*dMupzPh1 + min(reshape(obj.U{2}.z(:,:,1:Nz),N,1), 0).*dMupzPh2;
%                 vecZ2 = max(reshape(obj.U{1}.z(:,:,2:Nz+1),N,1), 0).*dMupzPh1 + max(reshape(obj.U{2}.z(:,:,2:Nz+1),N,1), 0).*dMupzPh2;
%                 acc = pv/dt .* (obj.drhoTdp .* obj.z(:,i));
%                 DiagVecs = [-vecZ2, -vecY2, -vecX2, vecZ2 + vecY2+vecX2-vecY1-vecX1-vecZ1+acc, vecX1, vecY1, vecZ1];
%                 DiagIndx = [-Nx*Ny, -Nx, -1, 0, 1, Nx, Nx*Ny];
%                 Jp{i} = Jp{i} + spdiags(DiagVecs, DiagIndx, N, N);
%                         
% 
%                 
%                 %% 2. Component i composition block
%                 for j=1:obj.NofComponents-1
%                     
%                     dMupxPh1 = obj.UpWind{1}.x*...
%                         ( obj.dMob(:, 1, j) .* obj.x(:,(i-1)*2+1) .* obj.rho(:,1)...
%                         + obj.Mob(:, 1) .* obj.dxdz(:,(i-1)*2+1, j) .* obj.rho(:,1)...
%                         + obj.Mob(:, 1) .* obj.x(:, (i-1)*2+1) .* obj.drhodz(:, 1));
%                     dMupyPh1 = obj.UpWind{1}.y*...
%                         ( obj.dMob(:, 1, j) .* obj.x(:, (i-1)*2+1) .* obj.rho(:,1)...
%                         + obj.Mob(:, 1) .* obj.dxdz(:, (i-1)*2+1, j) .* obj.rho(:,1)...
%                         + obj.Mob(:, 1) .* obj.x(:,(i-1)*2+1) .* obj.drhodz(:, 1));
%                     dMupzPh1 = obj.UpWind{1}.z*...
%                         ( obj.dMob(:, 1, j) .* obj.x(:,(i-1)*2+1) .* obj.rho(:,1)...
%                         + obj.Mob(:,1) .* obj.dxdz(:,(i-1)*2+1) .* obj.rho(:,1)...
%                         + obj.Mob(:,1) .* obj.x(:,(i-1)*2+1) .* obj.drhodz(:, 1));
%                     dMupxPh2 = obj.UpWind{2}.x*...
%                         ( obj.dMob(:, 2, j) .* obj.x(:,(i-1)*2+2) .* obj.rho(:,2)...
%                         + obj.Mob(:,2) .* obj.dxdz(:,(i-1)*2+2) .* obj.rho(:,2)...
%                         + obj.Mob(:,2) .* obj.x(:,(i-1)*2+2) .* obj.drhodz(:, 2));
%                     dMupyPh2 = obj.UpWind{2}.y*...
%                         ( obj.dMob(:, 2, j) .* obj.x(:,(i-1)*2+2) .* obj.rho(:,2)...
%                         + obj.Mob(:, 2) .* obj.dxdz(:,(i-1)*2+2, j) .* obj.rho(:,2)...
%                         + obj.Mob(:, 2) .* obj.x(:, (i-1)*2+2) .* obj.drhodz(:, 2));
%                     dMupzPh2 = obj.UpWind{2}.z*...
%                         ( obj.dMob(:, 2, j) .* obj.x(:, (i-1)*2+2) .* obj.rho(:,2)...
%                         + obj.Mob(:,2) .* obj.dxdz(:, (i-1)*2+2, j) .* obj.rho(:,2)...
%                         + obj.Mob(:,2) .* obj.x(:, (i-1)*2+2) .* obj.drhodz(:, 2));
%                     
%                     vecX1 = min(reshape(obj.U{1}.x(1:Nx,:,:),N,1), 0).*dMupxPh1 + min(reshape(obj.U{2}.x(1:Nx,:,:),N,1), 0).*dMupxPh2;
%                     vecX2 = max(reshape(obj.U{1}.x(2:Nx+1,:,:),N,1), 0).*dMupxPh1 + max(reshape(obj.U{2}.x(2:Nx+1,:,:),N,1), 0).*dMupxPh2;
%                     vecY1 = min(reshape(obj.U{1}.y(:,1:Ny,:),N,1), 0).*dMupyPh1 + min(reshape(obj.U{2}.y(:,1:Ny,:),N,1), 0).*dMupyPh2;
%                     vecY2 = max(reshape(obj.U{1}.y(:,2:Ny+1,:),N,1), 0).*dMupyPh1 + max(reshape(obj.U{2}.y(:,2:Ny+1,:),N,1), 0).*dMupyPh2;
%                     vecZ1 = min(reshape(obj.U{1}.z(:,:,1:Nz),N,1), 0).*dMupzPh1 + min(reshape(obj.U{2}.z(:,:,1:Nz),N,1), 0).*dMupzPh2;
%                     vecZ2 = max(reshape(obj.U{1}.z(:,:,2:Nz+1),N,1), 0).*dMupzPh1 + max(reshape(obj.U{2}.z(:,:,2:Nz+1),N,1), 0).*dMupzPh2;
%                     
%                     if i == obj.NofComponents
%                         acc = - pv/dt .* rhoT + pv/dt .* obj.z(:,i) .* obj.drhoTdz(:, j);
%                     elseif i == j
%                         acc = pv/dt .* rhoT + pv/dt .* obj.z(:,i) .* obj.drhoTdz(:, j);
%                     end
%                     
%                     DiagVecs = [-vecZ2, -vecY2, -vecX2, vecZ2+vecY2+vecX2-vecZ1-vecY1-vecX1+acc, vecX1, vecY1, vecZ1];
%                     DiagIndx = [-Nx*Ny, -Nx, -1, 0, 1, Nx, Nx*Ny];
%                     Jz{i,j} = spdiags(DiagVecs,DiagIndx, N, N);
%                     % Capillarity
%                     % Jz{i,j} = Jz{i,j} - obj.Tph{i,1} * spdiags(obj.dPc, 0, N, N);
%                     Jz{i,j} = Jz{i,j} - obj.Tph{2*i-1,1} * spdiags(obj.dPc, 0, N, N);
% 
%                 end
%             end
            

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            Jhorizontal = cell(obj.NofComponents, 1); 
            Nt = DiscretizationModel.N;
            Nm = DiscretizationModel.ReservoirGrid.N;
            
            for c = 1:obj.NofComponents
                Jph{c} = sparse(Nt, obj.NofComponents*Nt);
                % Reservoir
                Index.Start = 1;
                Index.End = Nm;
                [Jp_res, Jz_res] = BuildMediumJacobian(obj, ProductionSystem.Reservoir, DiscretizationModel.ReservoirGrid, dt, Index, 0, c);
                
                % Wells
                if (Time <= obj.Injection_time) % well shutdown control
                    [Jp_res, Jz_res] = obj.AddWellsToJacobian(Jp_res, Jz_res, obj.P(:,obj.NofPhases), obj.rho, obj.x, ProductionSystem.Wells, ProductionSystem.Reservoir.K, c);
                end
                  
                % Fractures
                Jp = [];  Jz = [];
                for f = 1 : ProductionSystem.FracturesNetwork.NumOfFrac
                    Nf = DiscretizationModel.FracturesGrid.N;
                    Index.Start = DiscretizationModel.Index_Local_to_Global( Nm, f, 1     );
                    Index.End   = DiscretizationModel.Index_Local_to_Global( Nm, f, Nf(f) );
                    [Jp_frac, Jz_frac] = BuildMediumJacobian(obj, ProductionSystem.FracturesNetwork.Fractures(f), DiscretizationModel.FracturesGrid.Grids(f), dt, Index, f, c);
                    Jp  = blkdiag(Jp, Jp_frac);
                    Jz  = blkdiag(Jz, Jz_frac);
                end
                
                Jp  = blkdiag(Jp_res,Jp);
                Jz  = blkdiag(Jz_res,Jz);
                
                % frac-matrix and frac-frac connections
                for i=1:length(DiscretizationModel.CrossConnections) % The length is equal to the number of fracture cells
                    if isempty(DiscretizationModel.CrossConnections(i).Cells),  continue;  end   
                    T_Geo = DiscretizationModel.CrossConnections(i).T_Geo;
                    UpWind = DiscretizationModel.CrossConnections(i).UpWind; % boolean vector (0 and 1)
                    idx_m = DiscretizationModel.CrossConnections(i).Cells; % idx_m and idx_f are global indexes; idx_m is a vector
                    idx_f = i + Nm;
                    
                    % 1. Pressure block
                    Jp_conn = - T_Geo .* (...
                             UpWind(:,1) .* obj.Mob(idx_m, 1) .* (obj.rho(idx_m,1).* obj.x(idx_m,(c-1)*2+1) + obj.rho(idx_m,1) .* obj.dxdp(idx_m,(c-1)*2+1) .* ( obj.P(idx_m,1) - obj.P(idx_f,1) ) + obj.drhodp(idx_m,1).*obj.x(idx_m,(c-1)*2+1).*( obj.P(idx_m,1) - obj.P(idx_f,1) )) + ...
                            ~UpWind(:,1) .* obj.Mob(idx_f, 1) .* (obj.rho(idx_f,1).* obj.x(idx_f,(c-1)*2+1) + obj.rho(idx_f,1) .* obj.dxdp(idx_f,(c-1)*2+1) .* ( obj.P(idx_m,1) - obj.P(idx_f,1) ) + obj.drhodp(idx_f,1).*obj.x(idx_f,(c-1)*2+1).*( obj.P(idx_m,1) - obj.P(idx_f,1))) + ...
                             UpWind(:,2) .* obj.Mob(idx_m, 2) .* (obj.rho(idx_m,2).* obj.x(idx_m,(c-1)*2+2) + obj.rho(idx_m,2) .* obj.dxdp(idx_m,(c-1)*2+2) .* ( obj.P(idx_m,2) - obj.P(idx_f,2) ) + obj.drhodp(idx_m,2).*obj.x(idx_m,(c-1)*2+2).*( obj.P(idx_m,2) - obj.P(idx_f,2) )) + ...
                            ~UpWind(:,2) .* obj.Mob(idx_f, 2) .* (obj.rho(idx_f,2).* obj.x(idx_f,(c-1)*2+2) + obj.rho(idx_f,2) .* obj.dxdp(idx_f,(c-1)*2+2) .* ( obj.P(idx_m,2) - obj.P(idx_f,2) ) + obj.drhodp(idx_f,2).*obj.x(idx_f,(c-1)*2+2).*( obj.P(idx_m,2) - obj.P(idx_f,2))) );
                        
                    % frac - mat or frac1 - frac2
                    Jp(idx_f, idx_m) = Jp_conn;
                    Jp(idx_f, idx_f) = Jp(idx_f, idx_f) - sum(Jp_conn);
                    % mat-frac or frac2 - frac1
                    Jp(idx_m, idx_f) = Jp_conn';
                    Jp(sub2ind([Nt, Nt], idx_m, idx_m)) = Jp(sub2ind([Nt, Nt], idx_m, idx_m)) - Jp_conn;

                    % 2. Composition block
                    % Jz1_conn: dqf(idx_f)/dz(idx_m) for upwind cell being matrix
                    Jz1_conn = T_Geo .* (...
                        UpWind(:,1) .* obj.rho(idx_m,1) .* ( obj.dMob(idx_m,1) .* obj.x(idx_m,(c-1)*2+1) + obj.Mob(idx_m,1) .* obj.dxdz(idx_m,(c-1)*2+1) ) .* (obj.P(idx_f,1) - obj.P(idx_m,1)) + ...
                        UpWind(:,2) .* obj.rho(idx_m,2) .* ( obj.dMob(idx_m,2) .* obj.x(idx_m,(c-1)*2+2) + obj.Mob(idx_m,2) .* obj.dxdz(idx_m,(c-1)*2+2) ) .* (obj.P(idx_f,2) - obj.P(idx_m,2)) );
                    % Jz2_conn: dqf(idx_f)/dz(idx_f) for upwind cell being frac
                    Jz2_conn = T_Geo .* (...
                       ~UpWind(:,1) .* obj.rho(idx_f,1) .* ( obj.dMob(idx_f,1) .* obj.x(idx_f,(c-1)*2+1) + obj.Mob(idx_f,1) .* obj.dxdz(idx_f,(c-1)*2+1) ) .* (obj.P(idx_f,1) - obj.P(idx_m,1)) + ...
                       ~UpWind(:,2) .* obj.rho(idx_f,2) .* ( obj.dMob(idx_f,2) .* obj.x(idx_f,(c-1)*2+2) + obj.Mob(idx_f,2) .* obj.dxdz(idx_f,(c-1)*2+2) ) .* (obj.P(idx_f,2) - obj.P(idx_m,2)) );
                      
                    % frac - mat or frac1 - frac2
                    Jz(idx_f, idx_m) = Jz1_conn;
                    Jz(idx_f, idx_f) = Jz(idx_f, idx_f) + sum(Jz2_conn);
                    % mat-frac or frac2 - frac1
                    Jz(idx_m, idx_f) = -Jz2_conn;
                    % diag of mat or frac2
                    Jz(sub2ind([Nt, Nt], idx_m, idx_m)) = Jz(sub2ind([Nt, Nt], idx_m, idx_m)) - Jz1_conn; 
                end               
  
                % Combine the Jacobian blocks
                Jhorizontal{c} = horzcat(Jp, Jz); 
            end

%            % 3. Add wells to each block
%             if (Time <= obj.Injection_time) % well shutdown control
%                 [Jp, Jz] = obj.AddWellsToJacobian(Jp, Jz, obj.P(:,obj.NofPhases), obj.rho, obj.x, ProductionSystem.Wells, ProductionSystem.Reservoir.K);
%             end
            
%            % Full Jacobian
%             if obj.NofComponents <= 2 
%             Jacobian = [Jp{1}, Jz{1,1}
%                         Jp{2}, Jz{2,1}];
%             else
%             % for now let's consider a max of 3 components!! 
%             Jacobian = [Jp{1}, Jz{1,1}, Jz{1,2};...
%                         Jp{2}, Jz{2,1}, Jz{2,2};...
%                         Jp{3}, Jz{3,1}, Jz{3,2}];
%             end

            Jacobian = Jhorizontal{1};
            %Jacobian_noconn = Jphnoconn{1};
            for c=2:obj.NofComponents
                Jacobian = vertcat(Jacobian, Jhorizontal{c});
            end

        end
        %%
%         function [Jp, Jz] = AddWellsToJacobian(obj, Jp, Jz, p, rho, x, Wells, K)
%             Inj = Wells.Inj;
%             Prod = Wells.Prod;
%             % Injectors
%             % for pressure conrol; (commented for rate control)
% %             for i=1:Wells.NofInj
% %                 a = Inj(i).Cells;
% %                 for j=1:length(a)
% %                     for c=1:obj.NofComponents
% %                     Jp{c}(a(j),a(j)) = Jp{c}(a(j),a(j)) ...
% %                                        + Inj(i).PI(j) * K(a(j)) * (Inj(i).Mob(1,1) * Inj(i).rho(j,1) * Inj(i).x(1,(c-1)*2+1) ...
% %                                        + Inj(i).Mob(1,2) *  Inj(i).rho(j,2) * Inj(i).x(1,(c-1)*2+2));
% %                     end
% %                 end
% %             end
%             
%             % Producers
%             for i=1:Wells.NofProd
%                 b = Prod(i).Cells;
%                 for j=1:length(b)
%                     for c=1:obj.NofComponents
%                         %Pressure blocks
%                         Jp{c}(b(j),b(j)) = Jp{c}(b(j),b(j))...
%                             + Prod(i).PI(j) * K(b(j)) *...
%                             ( obj.Mob(b(j), 1) * rho(b(j),1) * x(b(j), (c-1)*2+1) ...
%                             + obj.Mob(b(j), 2) * rho(b(j),2) * x(b(j), (c-1)*2+2)...
%                             )...
%                             - Prod(i).PI(j) * K(b(j)) * obj.Mob(b(j), 1) * x(b(j), (c-1)*2+1) * obj.drhodp(b(j),1) * (Prod(i).p(j) - p(b(j))) ...
%                             - Prod(i).PI(j) * K(b(j)) * obj.Mob(b(j), 1) * obj.dxdp(b(j), (c-1)*2+1) * rho(b(j),1) * (Prod(i).p(j) - p(b(j))) ...
%                             - Prod(i).PI(j) * K(b(j)) * obj.Mob(b(j), 2) * x(b(j), (c-1)*2+2) * obj.drhodp(b(j),2) * (Prod(i).p(j) - p(b(j)))...
%                             - Prod(i).PI(j) * K(b(j)) * obj.Mob(b(j), 2) * obj.dxdp(b(j), (c-1)*2+2) * rho(b(j),2) * (Prod(i).p(j) - p(b(j)));
%                         for zI = 1:obj.NofComponents-1
%                             Jz{c, zI}(b(j),b(j)) = Jz{c, zI}(b(j),b(j))...
%                                 - Prod(i).PI(j) * K(b(j)) *...
%                                 ( obj.dMob(b(j), 1, zI) * rho(b(j), 1) * x(b(j), (c-1)*2+1) ...
%                                 + obj.dMob(b(j), 2, zI) * rho(b(j), 2) * x(b(j), (c-1)*2+2)...
%                                 + obj.Mob(b(j), 1) * rho(b(j), 1) * obj.dxdz(b(j), (c-1)*2+1, zI) ...
%                                 + obj.Mob(b(j), 2) * rho(b(j), 2) * obj.dxdz(b(j), (c-1)*2+2, zI)...
%                                 + obj.Mob(b(j), 1, zI) * obj.drhodz(b(j), 1) * x(b(j), (c-1)*2+1) ...
%                                 + obj.Mob(b(j), 2, zI) * obj.drhodz(b(j), 2) * x(b(j), (c-1)*2+2)...
%                                 ) * (Prod(i).p(j) - p(b(j)));
%                         end
%                     end
%                 end          
%             end
%         end
        %%
        function [Jp, Jz] = AddWellsToJacobian(obj, Jp, Jz, p, rho, x, Wells, K, c)
            Prod = Wells.Prod;

            % Producers
            for i=1:Wells.NofProd
                b = Prod(i).Cells;
                for j=1:length(b)
                    %Pressure blocks
                    Jp(b(j),b(j)) = Jp(b(j),b(j))...
                        + Prod(i).PI(j) * K(b(j)) *...
                        ( obj.Mob(b(j), 1) * rho(b(j),1) * x(b(j), (c-1)*2+1) ...
                        + obj.Mob(b(j), 2) * rho(b(j),2) * x(b(j), (c-1)*2+2)...
                        )...
                        - Prod(i).PI(j) * K(b(j)) * obj.Mob(b(j), 1) * x(b(j), (c-1)*2+1) * obj.drhodp(b(j),1) * (Prod(i).p(j) - p(b(j))) ...
                        - Prod(i).PI(j) * K(b(j)) * obj.Mob(b(j), 1) * obj.dxdp(b(j), (c-1)*2+1) * rho(b(j),1) * (Prod(i).p(j) - p(b(j))) ...
                        - Prod(i).PI(j) * K(b(j)) * obj.Mob(b(j), 2) * x(b(j), (c-1)*2+2) * obj.drhodp(b(j),2) * (Prod(i).p(j) - p(b(j)))...
                        - Prod(i).PI(j) * K(b(j)) * obj.Mob(b(j), 2) * obj.dxdp(b(j), (c-1)*2+2) * rho(b(j),2) * (Prod(i).p(j) - p(b(j)));
                    for zI = 1:obj.NofComponents-1
                        Jz(b(j),b(j)) = Jz(b(j),b(j))...
                            - Prod(i).PI(j) * K(b(j)) *...
                            ( obj.dMob(b(j), 1, zI) * rho(b(j), 1) * x(b(j), (c-1)*2+1) ...
                            + obj.dMob(b(j), 2, zI) * rho(b(j), 2) * x(b(j), (c-1)*2+2)...
                            + obj.Mob(b(j), 1) * rho(b(j), 1) * obj.dxdz(b(j), (c-1)*2+1, zI) ...
                            + obj.Mob(b(j), 2) * rho(b(j), 2) * obj.dxdz(b(j), (c-1)*2+2, zI)...
                            + obj.Mob(b(j), 1, zI) * obj.drhodz(b(j), 1) * x(b(j), (c-1)*2+1) ...
                            + obj.Mob(b(j), 2, zI) * obj.drhodz(b(j), 2) * x(b(j), (c-1)*2+2)...
                            ) * (Prod(i).p(j) - p(b(j)));
                    end
                end          
            end
        end
        %%
        function delta = UpdateState(obj, delta, ProductionSystem, FluidModel, DiscretizationModel)
            if sum(isnan(delta))
                % if the solution makes no sense, skip this step
                return
            else
                Nm = DiscretizationModel.ReservoirGrid.N;
                Nt = DiscretizationModel.N;
                deltaP = delta(1:Nt);
                deltaz = delta(Nt+1: end);
                %% 1. Update matrix
                % 1.a Update Pressure
                Pm = ProductionSystem.Reservoir.State.Properties(['P_', num2str(obj.NofPhases)]);
                Pm.update(deltaP(1:Nm));
                % 1.b Update z
                DeltaLast = zeros(Nm, 1);
                for c = 1:obj.NofComponents-1
                    Zm = ProductionSystem.Reservoir.State.Properties(['z_', num2str(c)]);
                    % Deltaz = delta(c*Nt + 1:c*Nt + Nm);
                    Deltaz = deltaz(1:Nm);
                    
                    Zm.update(Deltaz);
                    Zm.Value = max(Zm.Value, 1.0e-8);
                    Zm.Value = min(Zm.Value, 1-1.0e-8);
                    DeltaLast = DeltaLast + Deltaz;
                end
                Zm = ProductionSystem.Reservoir.State.Properties(['z_', num2str(obj.NofComponents)]);
                Zm.update(-DeltaLast);
                Zm.Value = max(Zm.Value, 1.0e-8);
                Zm.Value = min(Zm.Value, 1-1.0e-8);
                % Update Remaining properties (x, S, rho, rhoT, Pc)
                obj.UpdatePhaseCompositions(ProductionSystem.Reservoir, DiscretizationModel, FluidModel, 1);
                
                %% 2. Update fractures pressure and densities
                if ProductionSystem.FracturesNetwork.Active
                    EP = Nm;
                    Nf = DiscretizationModel.FracturesGrid.N;
                    for f = 1:ProductionSystem.FracturesNetwork.NumOfFrac
                        IP = EP + 1;
                        EP = IP + Nf(f) - 1;
                        % 2.a Update Pressure
                        Pf = ProductionSystem.FracturesNetwork.Fractures(f).State.Properties(['P_', num2str(obj.NofPhases)]);
                        Pf.update(deltaP(IP:EP));
                        % 2.b Update z
                        DeltaLast = zeros(Nf(f), 1);
                        for c = 1:obj.NofComponents-1
                            IZ = Nt*(c-1) + IP;
                            EZ = Nt*(c-1) + EP;
                            Zf = ProductionSystem.FracturesNetwork.Fractures(f).State.Properties(['z_', num2str(c)]); 
                            % Zf.update(delta(c*Nf(f) + 1:(c+1)*Nf(f))); 
                            Deltaz = deltaz(IZ:EZ);

                            Zf.update(Deltaz);
                            % Remove values that are not physical
                            Zf.Value = max(Zf.Value, 1.0e-8);
                            Zf.Value = min(Zf.Value, 1-1.0e-8);
                            DeltaLast = DeltaLast + Deltaz;
                        end
                        Zf = ProductionSystem.FracturesNetwork.Fractures(f).State.Properties(['z_', num2str(obj.NofComponents)]);
                        Zf.update(-DeltaLast);
                        % Remove values that are not physical
                        Zf.Value = max(Zf.Value, 1.0e-8);
                        Zf.Value = min(Zf.Value, 1-1.0e-8);
                        % Update Remaining properties (x, S, rho, rhoT, Pc)
                        obj.UpdatePhaseCompositions(ProductionSystem.FracturesNetwork.Fractures(f), DiscretizationModel, FluidModel, 0);
                    end
                end
            end
        end
        %%
        function qf = ComputeSourceTerms_frac_mat(obj, ProductionSystem, DiscretizationModel)
%             qf = zeros(DiscretizationModel.N, obj.NofPhases);
%             Nm = DiscretizationModel.ReservoirGrid.N;
%             % Global variables
%             % P = ProductionSystem.CreateGlobalVariables([DiscretizationModel.ReservoirGrid; DiscretizationModel.FracturesGrid.Grids], obj.NofPhases, 'P_'); % useful for cross connections assembly
%             % rho = ProductionSystem.CreateGlobalVariables([DiscretizationModel.ReservoirGrid; DiscretizationModel.FracturesGrid.Grids], obj.NofPhases, 'rho_'); % useful for cross connections assembly
%             for ph=1:obj.NofPhases
%                 % fill in qf
%                 for c=1:length(DiscretizationModel.CrossConnections) % The length is equal to the number of fracture cells
%                     j = DiscretizationModel.CrossConnections(c).Cells; % j and i are global indexes
%                     i = c + Nm;
%                     T_Geo = DiscretizationModel.CrossConnections(c).T_Geo;
%                     UpWind = DiscretizationModel.CrossConnections(c).UpWind; % boolean vector (0 and 1)
%                     qf(i, ph) = qf(i, ph) + sum(T_Geo .* ...
%                         ( UpWind(:, ph) .* obj.Mob(j, ph) .* obj.rho(j, ph) .* (obj.P(j, ph) - obj.P(i, ph)) + ...
%                         ~UpWind(:, ph) .* obj.Mob(i, ph) .* obj.rho(i, ph) .* (obj.P(j, ph) - obj.P(i, ph))) );
%                     qf(j, ph) = qf(j, ph) + T_Geo .* ...
%                         ( UpWind(:, ph) .* obj.Mob(j, ph) .* obj.rho(j, ph) .* (obj.P(i, ph) - obj.P(j, ph)) + ...
%                         ~UpWind(:, ph) .* obj.Mob(i, ph) .* obj.rho(i, ph) .* (obj.P(i, ph) - obj.P(j, ph)) );
%                 end
%             end

            qf = zeros(DiscretizationModel.N, obj.NofComponents);
            Nm = DiscretizationModel.ReservoirGrid.N;

            for c=1:obj.NofComponents
                % fill in qf
                for i=1:length(DiscretizationModel.CrossConnections) % The length is equal to the number of fracture cells
                    T_Geo = DiscretizationModel.CrossConnections(i).T_Geo;
                    UpWind = DiscretizationModel.CrossConnections(i).UpWind; % boolean vector (0 and 1)
                    idx_m = DiscretizationModel.CrossConnections(i).Cells; % idx_m and idx_f are global indexes; idx_m is a vector
                    idx_f = i + Nm;
                    % qf = T_Geo * Upwind * Mob * rho * x * (dP)                    
                    qf(idx_f, c) = qf(idx_f, c) + sum(T_Geo .* ...
                        ( UpWind(:,1) .* obj.Mob(idx_m,1) .* obj.rho(idx_m,1) .*obj.x(idx_m,(c-1)*2+1) .* (obj.P(idx_m,1) - obj.P(idx_f,1)) + ...
                         ~UpWind(:,1) .* obj.Mob(idx_f,1) .* obj.rho(idx_f,1) .*obj.x(idx_f,(c-1)*2+1) .* (obj.P(idx_m,1) - obj.P(idx_f,1)))+ ...
                                                      T_Geo .* ...
                        ( UpWind(:,2) .* obj.Mob(idx_m,2) .* obj.rho(idx_m,2) .*obj.x(idx_m,(c-1)*2+2) .* (obj.P(idx_m,2) - obj.P(idx_f,2)) + ...
                         ~UpWind(:,2) .* obj.Mob(idx_f,2) .* obj.rho(idx_f,2) .*obj.x(idx_f,(c-1)*2+2) .* (obj.P(idx_m,2) - obj.P(idx_f,2))) );
                    
                    qf(idx_m, c) = qf(idx_m, c) + T_Geo .* ...
                        ( UpWind(:,1) .* obj.Mob(idx_m,1) .* obj.rho(idx_m,1) .*obj.x(idx_m,(c-1)*2+1) .* (obj.P(idx_f,1) - obj.P(idx_m,1)) + ...
                         ~UpWind(:,1) .* obj.Mob(idx_f,1) .* obj.rho(idx_f,1) .*obj.x(idx_f,(c-1)*2+1) .* (obj.P(idx_f,1) - obj.P(idx_m,1)))+ ...
                                                  T_Geo .*...
                        ( UpWind(:,2) .* obj.Mob(idx_m,2) .* obj.rho(idx_m,2) .*obj.x(idx_m,(c-1)*2+2) .* (obj.P(idx_f,2) - obj.P(idx_m,2)) + ...
                         ~UpWind(:,2) .* obj.Mob(idx_f,2) .* obj.rho(idx_f,2) .*obj.x(idx_f,(c-1)*2+2) .* (obj.P(idx_f,2) - obj.P(idx_m,2)));
                end
            end
        end
    end
end